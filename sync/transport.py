import requests

__author__ = 'nikolas'

class DataSourceTransport:

    headers = {
        "Content-Type": "application/json"
    }

    @classmethod
    def get(cls, url):
        response = requests.get(
            url=url,
            headers=cls.headers
        )
        return response.json()