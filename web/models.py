from django.contrib.auth.models import User
from django.db import models

from sync.transport import DataSourceTransport


class Consumer(models.Model):

    user = models.OneToOneField(User)
    wallet = models.CharField(max_length=255)

class DataSource(models.Model):
    api_endpoint = models.URLField()
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    contract_abi = models.TextField(default="")

    def __str__(self):
        return "{}<{}>".format(self.name, self.address)

    @property
    def sensor_url(self):
        return "{}sensors".format(self.api_endpoint)

    def sync_with_owner(self):
        sensors = DataSourceTransport.get(self.sensor_url)
        for sensor in sensors:
            data = dict(
                data_source=self,
                sensor_id=sensor['pk'],
                sensor_name=sensor['name'],
                dt_start=sensor['data_dt_start'],
                dt_end=sensor['data_dt_end'],
                fee=sensor['fee']
            )
            ds_sensor = DataSourceSensor.objects.update_or_create(
                defaults=data, data_source=self, sensor_id=sensor['pk']
            )


class DataSourceSensor(models.Model):
    data_source = models.ForeignKey(DataSource, related_name="sensors")
    sensor_id = models.PositiveIntegerField()
    sensor_name = models.CharField(max_length=255)
    dt_start = models.DateTimeField()
    dt_end = models.DateTimeField()
    fee = models.PositiveIntegerField()

    def mk_str_interval(self):
        return "{} - {}".format(self.dt_start.strftime("%Y-%m-%d %H:%M:%S"), self.dt_end.strftime("%Y-%m-%d %H:%M:%S"))

    class Meta:
        unique_together = ('data_source', 'sensor_id', )


class Transaction(models.Model):

    statuses = (
        (1, "HOLD"),
        (2, "SUCCESS"),
        (3, "FAIL"),
        (4, 'OUT_OF_DATE')
    )

    consumer = models.ForeignKey(Consumer)
    dt_created = models.DateTimeField(auto_now_add=True)
    data_source_sensor = models.ForeignKey(DataSourceSensor)
    dt_start = models.DateTimeField()
    dt_end = models.DateTimeField()
    cost = models.PositiveIntegerField(default=0)
    status = models.PositiveSmallIntegerField(choices=statuses, default=1)
