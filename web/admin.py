from django.contrib import admin
from django.contrib.admin.sites import NotRegistered
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from web.models import DataSource, DataSourceSensor, Consumer


@admin.register(DataSource)
class DataSourceAdmin(admin.ModelAdmin):
    list_display = ['name', 'api_endpoint', 'address']


@admin.register(DataSourceSensor)
class DataSourceSensorAdmin(admin.ModelAdmin):
    list_display = ['data_source', 'sensor_id', 'sensor_name', 'dt_start', 'dt_end', 'fee']


class ConsumerAdminInline(admin.StackedInline):
    model = Consumer
    can_delete = False
    verbose_name_plural = 'employee'


class UserAdmin(UserAdmin):
    list_display = ['pk', 'username']
    inlines = (ConsumerAdminInline, )


try:
    admin.site.unregister(User)
except NotRegistered:
    pass
admin.site.register(User, UserAdmin)

# @admin.register(Consumer)
# class Consumer(admin.ModelAdmin):
    # list_display = ['pk', 'username', 'name', 'email', 'wallet']