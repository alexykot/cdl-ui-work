# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Consumer',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('wallet', models.CharField(max_length=255)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='DataSource',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('api_endpoint', models.URLField()),
                ('name', models.CharField(max_length=255)),
                ('address', models.CharField(max_length=255)),
                ('contract_abi', models.TextField(default='')),
            ],
        ),
        migrations.CreateModel(
            name='DataSourceSensor',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('sensor_id', models.PositiveIntegerField()),
                ('sensor_name', models.CharField(max_length=255)),
                ('dt_start', models.DateTimeField()),
                ('dt_end', models.DateTimeField()),
                ('fee', models.PositiveIntegerField()),
                ('data_source', models.ForeignKey(related_name='sensors', to='web.DataSource')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='datasourcesensor',
            unique_together=set([('data_source', 'sensor_id')]),
        ),
    ]
