# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('dt_created', models.DateTimeField(auto_now_add=True)),
                ('dt_start', models.DateTimeField()),
                ('dt_end', models.DateTimeField()),
                ('cost', models.PositiveIntegerField(default=0)),
                ('status', models.PositiveSmallIntegerField(choices=[(1, 'HOLD'), (2, 'SUCCESS'), (3, 'FAIL'), (4, 'OUT_OF_DATE')], default=1)),
                ('consumer', models.ForeignKey(to='web.Consumer')),
                ('data_source_sensor', models.ForeignKey(to='web.DataSourceSensor')),
            ],
        ),
    ]
