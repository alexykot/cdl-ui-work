from crispy_forms.bootstrap import StrictButton
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout
from django import forms
from django.forms.widgets import DateTimeInput

__author__ = 'nikolas'


class BuyForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super(BuyForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        # self.helper.form_id = 'id-exampleForm'
        # self.helper.form_class = 'blueForms'
        self.helper.form_method = 'post'
        # self.helper.form_action = '/buy'
        self.helper.form_class = 'form-inline'
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'
        self.helper.layout = Layout(
            'dt_start',
            'dt_end',
            Submit('submit', 'Submit'),
        )
    dt_start = forms.DateTimeField(
        label='End Interval',
        required=True,
    )

    dt_end = forms.DateTimeField(
        label='End Interval',
        required=True,
    )