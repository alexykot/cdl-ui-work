import datetime
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import AnonymousUser
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.views.generic import TemplateView, View
from django.shortcuts import redirect, render_to_response
from blockchain.entity import Contract
from web.forms import BuyForm
from web.models import DataSource, DataSourceSensor


class IndexView(TemplateView):
    template_name = "datasources.html"

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        result = []
        data_sources = DataSource.objects.all()

        for data_source in data_sources:
            sensors = data_source.sensors.all()
            for sensor in sensors:
                data = dict(
                    address=data_source.address,
                    name=data_source.name,
                    sensor=sensor.sensor_name,
                    interval=sensor.mk_str_interval(),
                    fee="{} whei".format(sensor.fee),
                    pk=sensor.pk
                )
                result.append(data)

        if isinstance(self.request.user, AnonymousUser):
            current_user = False
        else:
            current_user = self.request.user

        context.update({
            "data_sources": result,
            "current_user": current_user
        })
        return context


def login_view(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return redirect(reverse('index'))
        else:
            return render_to_response("error.html", dict(error="Account is disabled"))
    else:
        return render_to_response("error.html", dict(error="Invalid login or password"))

@login_required()
def buy_data(request, ds_pk):
    current_user = request.user
    errors = False
    buy_form = BuyForm()
    dss = DataSourceSensor.objects.get(pk=ds_pk)
    data = dict(
        address=dss.data_source.address,
        name=dss.data_source.name,
        sensor=dss.sensor_name,
        interval=dss.mk_str_interval(),
        fee="{} whei".format(dss.fee),
        pk=dss.pk
    )

    if request.method == 'POST':
        buy_form = BuyForm(request.POST)
        if buy_form.is_valid():
            dt_start = buy_form.cleaned_data['dt_start']
            dt_end = buy_form.cleaned_data['dt_end']

            delta = dt_start - dt_end
            cost = delta.seconds * dss.fee

            user = current_user
            dt_start = int(dt_start.replace(tzinfo=datetime.timezone.utc).timestamp())
            dt_end = int(dt_end.replace(tzinfo=datetime.timezone.utc).timestamp())
            contract = Contract(dss.data_source.address, dss.data_source.contract_abi)
            resp = contract.transact(user.wallet, "holdTransact", dss.sensor_id, dt_start, dt_end, cost)
            if resp != '0x':
                pass
                # Transaction

    return render_to_response(
        "data.html",
        {'form_data': buy_form, "data_source": data, 'current_user': current_user, "errors": errors},
        context_instance=RequestContext(request)
    )

