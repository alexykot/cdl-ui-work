from django.core.management import BaseCommand

from web.models import DataSource

__author__ = 'nikolas'

class Command(BaseCommand):
    def handle(self, *args, **options):
        data_sources = DataSource.objects.all()
        for data_source in data_sources:
            data_source.sync_with_owner()